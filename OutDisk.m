function [ faces, vertices ] = OutDisk( R, n, r, k, NC, VertOutCirc )
%OutDisk генерирует сетку для диска
%   R - радиус диска
%   n - количество точек на внешней окружности
%   NC - количество окружностей в на диске, включая внешнюю
%   VertOutCirc - вершины внешней окружности
% R = 10;
% n = 50;
% NC = 6;
R
n
r
k
NC
c = linspace(0, 1, NC);
Rn = k + floor(c* (n-k) ) %надо покумекать как округлять, floor, round, fix -- какой разнообразный мир
% Rn(1) = 1;%в центре выставляем одну точку. ноль -- можно, но не очень красиво получится
% x = zeros(sum(Rn), 1);
% y = zeros(sum(Rn), 1);
% Rn
% NC
% x = [];
% y = [];
x = VertOutCirc(:,1); % крепим вершины внешней окружности к другим вершинам окружностей
y = VertOutCirc(:,2);
for i=1:NC-1
    alpha = linspace(0, 2*pi, Rn(i)+1);
    xc = ((R-r)*c(i)+r)*sin(alpha(1:Rn(i)));
    yc = ((R-r)*c(i)+r)*cos(alpha(1:Rn(i)));
    x = [x; xc'];
    y = [y; yc'];
end
% Rn(1)
[f, v] = Disk(r, k, 5, [x(n+1:n+k) y(n+1:n+k) VertOutCirc(1,3)*ones(k)]);% ( R, n, NC, VertOutCirc )
% figure();
% trisurf(f, v(:,1), v(:,2), v(:,3));
% size(v)
x = [x; v(k+1:end, 1)]; %удалить потом нафиг!
y = [y; v(k+1:end, 2)];
faces = delaunay(x, y);
% ind1 = find(faces(:, 1) = x(1:k))
% ind2 = find(faces(:,2)) 
% figure();
% trisurf(faces, x, y, zeros(size(x)));
vertices = [x y VertOutCirc(1,3)*ones(size(x))];

end



