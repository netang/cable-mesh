function [ faces, vertices ] = CableMesh(L1, L2, NL1, NL2, R, NR, r, Nr, n, k)
%   Функция генерирует сетку для кабеля.
%   Принятые обозначения
%   L1 - длина первой части кабеля
%   L2 - длина второй части кабеля
%   NL1 - дискретизация (по L1) первой части кабеля
%   NL2 - дискретизация (по L2) второй части кабеля
%   R - радиус кабеля
%   NR - количество окружностей на внешнем кольцевом диске
%   r - радиус внутреннего провода
%   Nr - количество окружностей на внутрннем кольцевом диске
%   n - количество точек на окружности кабеля
%   k - количество точек на внутренней окружности кабеля
%   запускать: CableMesh(160, 50, 10, 5, 5, 50, 25);
% CableMesh(160, 60, 50, 18, 10, 5, 5, 5, 50, 25);

% R = 10;
% l = 5;
% L = 5;
% disp(['Длина кабеля: ' L '   NL = ' NL]) %м-о попробовать table
% n = 50;% how many point lies on cicle
alpha = linspace(0, 2*pi, n+1); %0 degree equals 180 degress -> n+1
alpha = alpha(1:end-1)'; % разбиваем окружность на n частей. (углы, углы, углы)
% teta = 2*pi/(n); % радиан на две соседние вершины.


%строим окружность
x = R*sin(alpha); 
y = R*cos(alpha);
%вытягиваем её по оси Z
z1 = linspace(0, L1, NL1);
z2 = linspace(L1, L1+L2, NL2);
% z = 0:Lh:L;
z = [z1 z2(2:end)];
Zn = length(z); % количество слоев по оси Z;
vertices = zeros(n*Zn + k*Zn, 3); % готовим массив под наши вершины
% n*Zn - вершины поверхности кабеля (без боковых дисков)
% k*Zn - вершины поверхности внутреннего провода кабеля


tmp_z = z(ones(1,n),:); %также можно через repmat(z,k,1);
tmp_z = tmp_z(:);
vertices(1:n*Zn,:) = [ repmat([x y], NL1+(NL2-1), 1) tmp_z];



alpha2 = linspace(0, 2*pi, k+1); % 0 degree equals 180 degress => k+1
alpha2 = alpha2(1:end-1)'; % разбиваем окружность на n частей. (углы, углы, углы)
% teta2 = 2*pi/(k); % радиан на две соседние вершины.
%строим окружность
x = r*sin(alpha2); 
y = r*cos(alpha2);


tmp_z = z(ones(1,k),:); %также можно через repmat(z,k,1);
tmp_z = tmp_z(:);
vertices(Zn*n+1:end,:) = [ repmat([x y], NL1+(NL2-1), 1) tmp_z];





faces = zeros( (Zn-1)*n*2+(Zn-1)*k*2, 3 );
%структура такова: боковая поверхность цилиндра -> боковая поверхность
%внутреннего цилиндра -> нижний диск
% (Zn-1)*n*2 - кол-во треугольников на поверхности кабеля (без дисков)
% (Zn-1)*k*2 - кол-во треугольников на поверхности внутреннего провода (без дисков)

% обозначаем грани для треугольников боковой поверхности внешнего цилиндра
%--------------------------------------------------------------------------
tic;
ind = 1:n-1;
M = repmat(ind, Zn-1, 1);
nv = 0:n:(Zn-2)*n;
A = M + repmat(nv', 1, n-1);   %вершины с номером i
A = A';
B = A + 1;                     %вершины с номером i + 1 
C = (A + n);                   %вершины с номером i + n
D = (C + 1);                   %вершины с номером i + n + 1
testface = [ A(:)      B(:)      C(:);
             C(:)      D(:)      B(:);
             B(end,:)' A(1,:)'   D(end,:)'
             D(end,:)' C(1,:)'   A(1,:)'];
t4 = toc;
%--------------------------------------------------------------------------

% tic;   генерация фейсисов для боковой поверхности, через циклы ненужно
kk = 1;
for i = 1:Zn-1
    for j = 1:n-1
        faces(kk,1) = (i-1)*n+j;
        faces(kk,2) = (i-1)*n+j+1;
        faces(kk,3) = (i-1)*n+n+j;
        kk = kk + 1;
        faces(kk,1) = (i-1)*n+n+j;
        faces(kk,2) = (i-1)*n+n+j+1;
        faces(kk,3) = (i-1)*n+j+1;
        kk = kk + 1;
    end
end

for i = 1:Zn-1
        faces(kk,1) = (i-1)*n+n;
        faces(kk,2) = (i-1)*n+1;
        faces(kk,3) = (i-1)*n+n+n;
        kk = kk + 1;
        faces(kk,1) = (i-1)*n+n+n;
        faces(kk,2) = (i-1)*n+n+1;
        faces(kk,3) = (i-1)*n+1;
        kk = kk + 1;
end
t5 = toc;
kol_facesov_bok_pov = kk-1
disp('количество вершин на боковой поверхности');
Zn*n
disp('количество граней на боковой поверхности');
(Zn-1)*2*n
% t5/t4
% size(testface)
% kk-1
% max(abs(testface - faces(1:kk-1,:)))

% figure();
% trisurf(faces(1:kk-1), vertices(:,1), vertices(:,2), vertices(:,3));
kk
tic;
ind = 1:k-1; %генерируем индексы вершин первого слоя внутреннего цилиндра
ind = ind + Zn*n;
ind
M = repmat(ind, Zn-1, 1);
nv = 0:k:(Zn-2)*k;
% size(M)
% size()
A = M + repmat(nv', 1, k-1);   %вершины с номером i
A = A';
B = A + 1;                     %вершины с номером i + 1 
C = (A + k);                   %вершины с номером i + k
D = (C + 1);                   %вершины с номером i + n + 1
testface2 = [ testface;
              A(:)      B(:)      C(:);
              C(:)      D(:)      B(:);
             B(end,:)' A(1,:)'   D(end,:)'
             D(end,:)' C(1,:)'   A(1,:)'];
t6 = toc;
%--------------------
for i = 1:Zn-1
    for j = 1:k-1
        faces(kk,1) = (i-1)*k+j + n*Zn;
        faces(kk,2) = (i-1)*k+j+1 + n*Zn;
        faces(kk,3) = (i-1)*k+k+j + n*Zn;
        kk = kk + 1;
%         faces(kk,1) = (i-1)*k+k+j + n*Zn;
%         faces(kk,2) = (i-1)*k+k+j+1 + n*Zn;
%         faces(kk,3) = (i-1)*k+j+1 + n*Zn;
%         kk = kk + 1;
    end
end
for i = 1:Zn-1
    for j = 1:k-1
%         faces(kk,1) = (i-1)*k+j + n*Zn;
%         faces(kk,2) = (i-1)*k+j+1 + n*Zn;
%         faces(kk,3) = (i-1)*k+k+j + n*Zn;
%         kk = kk + 1;
        faces(kk,1) = (i-1)*k+k+j + n*Zn;
        faces(kk,2) = (i-1)*k+k+j+1 + n*Zn;
        faces(kk,3) = (i-1)*k+j+1 + n*Zn;
        kk = kk + 1;
    end
end
for i = 1:Zn-1
        faces(kk,1) = (i-1)*k+k + n*Zn;
        faces(kk,2) = (i-1)*k+1 + n*Zn;
        faces(kk,3) = (i-1)*k+k+k + n*Zn;
        kk = kk + 1;
%         faces(kk,1) = (i-1)*k+k+k + n*Zn;
%         faces(kk,2) = (i-1)*k+k+1 + n*Zn;
%         faces(kk,3) = (i-1)*k+1 + n*Zn;
%         kk = kk + 1;
end
for i = 1:Zn-1
%         faces(kk,1) = (i-1)*k+k + n*Zn;
%         faces(kk,2) = (i-1)*k+1 + n*Zn;
%         faces(kk,3) = (i-1)*k+k+k + n*Zn;
%         kk = kk + 1;
        faces(kk,1) = (i-1)*k+k+k + n*Zn;
        faces(kk,2) = (i-1)*k+k+1 + n*Zn;
        faces(kk,3) = (i-1)*k+1 + n*Zn;
        kk = kk + 1;
end

disp('difference betwen')
max(abs(testface2((Zn-1)*2*n+1:kk-1,:) - faces((Zn-1)*2*n+1:kk-1,:)))
% figure();
% trisurf(testface2(1:(Zn-1)*n*2, :), vertices(:,1), vertices(:,2), vertices(:,3));
% figure();
% trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3));





%Генерация нижнего диска и сцепка последнего к кабелю.
%---------------------------------------------------
[f, v] = OutDisk(R, n, r, k, NR, vertices(1:n,:));
vertices = [vertices; v(k+n+1:end, :)];
tic;
indexes = find( f > n+k );
f(indexes) = f(indexes) + (n*Zn+k*Zn-n-k);
indexes = find( f > n  & f <= n+k );
f(indexes) = f(indexes) + (n*Zn-n);
faces = [faces; f];
find_time = toc;
%----------------------------------------------------

% Генерация верхнего диска и сцепка последнего к кабелю.
%---------------------------------------------------
[f, v] = OutDisk(R, n, r, k, NR, vertices((Zn-1)*n+1:Zn*n,:));% (Zn-1)*n+1:Zn*n - вершины последнего кольца внешней боковой поверхности цилиндра
size_vert = size(vertices, 1);
vertices = [vertices; v(k+n+1:end, :)];
tic;
indexes = find( f > n+k );
f(indexes) = f(indexes) + ( size_vert-n-k );
indexes = find( f <= n);
f(indexes) = f(indexes) + ( (Zn-1)*n );
indexes = find( f > n  & f <= n+k );
f(indexes) = f(indexes) + (n*Zn+(Zn-1)*k-n);
faces = [faces; f];
find_time = toc;
%----------------------------------------------------

%Генерация центрального диска и сцепка последнего к кабелю.
%---------------------------------------------------
[f, v] = OutDisk(R, n, r, k, NR, vertices((NL1-1)*n+1:NL1*n,:));% (Zn-1)*n+1:Zn*n - вершины последнего кольца внешней боковой поверхности цилиндра
size_vert = size(vertices, 1);
vertices = [vertices; v(k+n+1:end, :)];
tic;
indexes = find( f > n+k );
f(indexes) = f(indexes) + ( size_vert-n-k );
indexes = find( f <= n);
f(indexes) = f(indexes) + ( (NL1-1)*n );
indexes = find( f > n  & f <= n+k );
f(indexes) = f(indexes) + (n*Zn+(NL1-1)*k-n);
faces = [faces; f];
find_time = toc;
%----------------------------------------------------


figure();
trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);

end

