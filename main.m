%   Принятые обозначения
%   L - длина канала
%   R - радиус канала
%   n - количество точек на окружности канала
   




R = 10;
l = 5;
L = 5;
n = 50% how many point lies on cicle
alpha = linspace(0, 2*pi, n+1); % wron? because 0 degree equals 180 degress
alpha = alpha(1:end-1); % разбиваем окружность на n частей. (углы, углы, углы)
teta = 2*pi/(n); % радиан на две соседние вершины.


% Ll = 2*pi*R; % cicle lenght 
% d = L/(n-1)

%строим окружность
x = R*sin(alpha); 
y = R*cos(alpha);
%вытягиваем её по оси Z
z = 0:0.5:L;

% TRI = delaunay(x, y); 
% trisurf(TRI, x, y, zeros(size(x)));

Zn = length(z); % количество слоев по оси Z;
vertices = zeros(n*Zn, 3); % готовим массив под наши вершины
% n*Zn - вершины поверхности канала без боковых дисков
% 
%
for i = 1:Zn
    vertices((i-1)*n+1:(i-1)*n+n, :) = [x' y' z(i)*ones(n,1)]; %заполняем вершины дисков
end

faces = zeros( (Zn-1)*n*2, 3 );
% (Zn-1)*n*2 - кол-во треугольников на поверхности канала (без дисков)


k = 1;
for i = 1:Zn-1
    for j = 1:n-1
        faces(k,1) = (i-1)*n+j;
        faces(k,2) = (i-1)*n+j+1;
        faces(k,3) = (i-1)*n+n+j;
        k = k + 1;
        faces(k,1) = (i-1)*n+n+j;
        faces(k,2) = (i-1)*n+n+j+1;
        faces(k,3) = (i-1)*n+j+1;
        k = k + 1;
    end

end

for i = 1:Zn-1
        faces(k,1) = (i-1)*n+n;
        faces(k,2) = (i-1)*n+1;
        faces(k,3) = (i-1)*n+n+n;
        k = k + 1;
        faces(k,1) = (i-1)*n+n+n;
        faces(k,2) = (i-1)*n+n+1;
        faces(k,3) = (i-1)*n+1;
        k = k + 1;
end


[v f] = Disk(1, 1, 1, vertices(1:n,:));

vertices = [vertices; v(n+1:end, :)];

f(find(f > n)) = f(find(f > n)) + (n*Zn-n);
faces = [faces; f];
min(f)
length(vertices)

% length(f)
% n
% error(1);
% faces = [faces(1:1000, :); f];



figure();
trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3))

% r = R*cos(pi/n) %растояние от центра окружности до центра отрезка двух соседних вершин
% e = R - r % расстояние от центра отрезка между двумя вершинами до окружности
% a = 2*R*sin(pi/n)
% h = a*sqrt(3)/2 %высота равностороннего треугольника
% l = R - h - e
% for i=1:n
%     vertices(n*Zn+i, 1) = l*sin((i-0.5)*teta); % вершины треугольников нижнего дика
%     vertices(n*Zn+i, 2) = l*cos((i-0.5)*teta);
%     vertices(n*Zn+i, 3) = 0; % z zero layer
% end
% for i=1:n-1
%     faces((Zn-1)*n*2+i, 1) = i;
%     faces((Zn-1)*n*2+i, 2) = i+1;
%     faces((Zn-1)*n*2+i, 3) = n*Zn+i;
% end
% faces( (Zn-1)*n*2 + n, 1) = n;
% faces( (Zn-1)*n*2 + n, 2) = 1;
% faces( (Zn-1)*n*2 + n, 3) = n*Zn+n;
% 
% for i=1:n-1
%     faces((Zn-1)*n*2 + n + i, 1) = i+1;
%     faces((Zn-1)*n*2 + n + i, 2) = n*Zn+i;
%     faces((Zn-1)*n*2 + n + i, 3) = n*Zn+i+1;
% end
% faces((Zn-1)*n*2 + n + n, 1) = 1;
% faces((Zn-1)*n*2 + n + n, 2) = n*Zn+n;
% faces((Zn-1)*n*2 + n + n, 3) = n*Zn+1;
% 
% figure();
% trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3));
% 
% 
% 
% r=linspace(0, 1, n+1);
% NR=floor(2*pi*r*n);
% r=r*R;
% NR(1)=1;
% 
% for i=1:n
%     t=linspace(0,2*pi,NR(i)+1);
%     t=squeeze(t(1:NR(i)));
%     y=r(i)*cos(t)
%     x=r(i)*sin(t)
% end; 

