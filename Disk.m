function [ faces, vertices ] = Disk( R, n, NC, VertOutCirc )
%DISK генерирует сетку для диска
%   R - радиус диска
%   n - количество точек на внешней окружности
%   NC - количество окружностей в сетке, включая внешнюю
%   VertOutCirc - вершины внешней окружности
% R = 10;
% n = 50;
% NC = 6;
r = linspace(0, 1, NC);
Rn = floor(r*n); %надо покумекать как округлять, floor, round, fix -- какой разнообразный мир
Rn(1) = 1;%в центре выставляем одну точку. ноль -- можно, но не очень красиво получится
% x = zeros(sum(Rn), 1);
% y = zeros(sum(Rn), 1);
Rn
NC
% x = [];
% y = [];
x = VertOutCirc(:,1); % крепим вершины внешней окружности к другим вершинам окружностей
y = VertOutCirc(:,2);
for i=1:NC-1
    alpha = linspace(0, 2*pi, Rn(i)+1);
    xc = R*r(i)*sin(alpha(1:Rn(i)));
    yc = R*r(i)*cos(alpha(1:Rn(i)));
    x = [x; xc'];
    y = [y; yc'];
end


faces = delaunay(x, y); 
% trisurf(faces, x, y, zeros(size(x)));
vertices = [x y VertOutCirc(1,3)*ones(size(x))];

end

